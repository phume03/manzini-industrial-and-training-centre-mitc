﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models.SchoolViewModels
{
    public class EnrollmentDateGroup
    {
        [DataType(DataType.Date)]
        [Display(Name ="Date of Enrollment: ")]
        public DateTime? EnrollmentDate { get; set; }
        [Display(Name ="Enrolled Students (#): ")]
        public int StudentCount { get; set; }
    }
}
