﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models.SchoolViewModels
{
    public class AssignedCourseData
    {
        public int CourseID { get; set; }
        public string CourseName { get; set;}  
        public bool Assigned { get; set; }
    }
}
