﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models.SchoolViewModels
{
    // Custom Data Model to Contain Data from Three Tables
    public class InstructorIndexData
    {
        public PaginatedList<Instructor> Instructors { get; set; }
        public IEnumerable<Course> Courses { get; set; }
        public IEnumerable<Enrollment> Enrollments { get; set; }
    }
}
