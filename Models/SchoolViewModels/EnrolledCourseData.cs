﻿namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models.SchoolViewModels
{
    public class EnrolledCourseData
    {
        public int CourseID { get; set; }
        public string CourseName { get; set; }
        public bool Enrolled { get; set; }
    }
}
