﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models
{
    public class Student
    {
        public int StudentId { get; set; }
        [Required, Display(Name = "First Name: "),StringLength(50, MinimumLength = 1)]
        public string StudentFirstName { get; set; }
        [Required, Display(Name = "Last Name: "),StringLength(50, MinimumLength = 2)]
        public string StudentLastName { get; set; }
        [DataType(DataType.Date), Display(Name = "Date Enrolled: "),DisplayFormat(DataFormatString ="{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime EnrollmentDate { get; set; }
        [Display(Name = "Full Name: ")]
        public string FullName
        {
            get
            {
                return StudentLastName + ", " + StudentFirstName;
            }
        }

        [Display(Name = "Courses Enrolled In: ")]
        public ICollection<Enrollment> Enrollments { get; set; }

        [DataType(DataType.DateTime),Display(Name = "Date Created: ")]
        public DateTime DateCreated { get; set; }
        [DataType(DataType.DateTime), Display(Name = "Date Modified: ")]
        public DateTime DateModified { get; set; }
        [DataType(DataType.DateTime), Display(Name = "Date Deleted: ")]
        public DateTime? DateDeleted { get; set; }
    }
}
