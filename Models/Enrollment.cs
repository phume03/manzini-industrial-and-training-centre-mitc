﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models
{
    public enum GRADE
    {
        NOGRADE, A, B, C, D, E, F
    }

    public class Enrollment
    {
        public int EnrollmentID { get; set; }
        public int CourseID { get; set; }
        public int StudentID { get; set; }

        [Display(Name = "Course Grade: "),DisplayFormat(NullDisplayText = "No grade")]
        public GRADE? Grade { get; set; }

        public Course Course { get; set; }
        public Student Student { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateModified { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? DateDeleted { get; set; }
    }
}
