﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models
{
    public class Instructor
    {
        public int InstructorID { get; set; }
        [Required,Display(Name = "Last Name: "),StringLength(50)]
        public string LastName { get; set; }
        [Required,Column("FirstName"),Display(Name = "First Name: "),StringLength(50)]
        public string FirstName { get; set; }
        [DataType(DataType.Date),DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),Display(Name = "Hire Date: ")]
        public DateTime HireDate { get; set; }

        [Display(Name = "Full Name: ")]
        public string FullName
        {
            get { 
                return LastName + ", " + FirstName; 
            }
        }

        [Display(Name ="Assigned Courses: ")]
        public ICollection<CourseAssignment> CourseAssignments { get; set; }
        [Display(Name = "Office: ")]
        public OfficeAssignment OfficeAssignment { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateModified { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? DateDeleted { get; set; }
    }
}
