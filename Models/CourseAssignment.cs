﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models
{
    public class CourseAssignment
    {
        public int CourseId { get; set; }
        public int InstructorId { get; set; }

        public Course Course { get; set; }
        public Instructor Instructor { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateModified { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? DateDeleted { get; set; }
    }
}
