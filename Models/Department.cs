﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models
{
    public class Department
    {
        public int DepartmentID { get; set; }
        [StringLength(50, MinimumLength = 3),Display(Name ="Department: ")]
        public string DepartmentName { get; set; }
        [DataType(DataType.Currency),Column(TypeName = "money"),Display(Name ="Department Budget (SZL): ")]
        public decimal Budget { get; set; }
        [DataType(DataType.Date),DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [Display(Name ="Department Administrator: ")]
        public int? InstructorID { get; set; }

        public Instructor Administrator { get; set; }
        [Display(Name ="Related Courses: ")]
        public ICollection<Course> Courses { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateModified { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? DateDeleted { get; set; }
    }
}
