﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models
{
    public class Course
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None),Display(Name = "Course Number: ")]
        public int CourseId { get; set; }
        [Display(Name ="Course Name: ")]
        [StringLength(80, MinimumLength = 3)]
        public string CourseName { get; set; }
        [Display(Name = "Credit: "),Range(0, 5)]
        public int CourseCredit { get; set; }
        [Display(Name ="Department: ")]
        public int DepartmentID { get; set; }

        public Department Department { get; set; }
        [Display(Name = "Students Enrolled: ")]
        public ICollection<Enrollment> Enrollments { get; set; }
        [Display(Name ="Assigned Instructors: ")]
        public ICollection<CourseAssignment> CourseAssignments { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateModified { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? DateDeleted { get; set; }

    }
}
