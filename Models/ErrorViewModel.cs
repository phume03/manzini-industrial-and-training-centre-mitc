using System;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
