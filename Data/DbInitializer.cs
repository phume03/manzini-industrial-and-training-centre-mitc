﻿using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models;
using System;
using System.Linq;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Data
{
    public static class DbInitializer
    {
        public static void Initializer(CentreContext context)
        {
            context.Database.EnsureCreated();

            if (context.Students.Any())
            {
                return;
            }

            var students = new Student[]
            {
                new Student { StudentFirstName="Carson",StudentLastName="Alexander",EnrollmentDate=DateTime.Parse("2005-09-01"), DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Student { StudentFirstName="Meredith",StudentLastName="Alonso",EnrollmentDate=DateTime.Parse("2002-09-01"), DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Student { StudentFirstName="Arturo",StudentLastName="Anand",EnrollmentDate=DateTime.Parse("2003-09-01"), DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Student { StudentFirstName="Gytis",StudentLastName="Barzdukas",EnrollmentDate=DateTime.Parse("2002-09-01"), DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Student { StudentFirstName="Yan",StudentLastName="Li",EnrollmentDate=DateTime.Parse("2002-09-01"), DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Student { StudentFirstName="Peggy",StudentLastName="Justice",EnrollmentDate=DateTime.Parse("2001-09-01"), DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Student { StudentFirstName="Laura",StudentLastName="Norman",EnrollmentDate=DateTime.Parse("2003-09-01"), DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Student { StudentFirstName="Nino",StudentLastName="Olivetto",EnrollmentDate=DateTime.Parse("2005-09-01"), DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Student { StudentFirstName="Jabulani",StudentLastName="Magongo",EnrollmentDate=DateTime.Parse("2022-01-11"), DateCreated=DateTime.Now, DateModified=DateTime.Now}
            };

            foreach(Student student in students)
            {
                context.Students.Add(student);
            }
            context.SaveChanges();

            if (context.Instructors.Any())
            {
                return;
            }

            var instructors = new Instructor[]
            {
                new Instructor { LastName="Abercrombie", FirstName="Kim", HireDate=DateTime.Parse("1995-03-11"), DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Instructor { LastName="Fakhouri", FirstName="Fadi", HireDate=DateTime.Parse("2002-07-06"), DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Instructor { LastName="Harui", FirstName="Roger", HireDate=DateTime.Parse("1998-07-01"), DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Instructor { LastName="Kapoor", FirstName="Candace", HireDate=DateTime.Parse("2001-01-15"), DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Instructor { LastName="Zheng", FirstName="Roger", HireDate=DateTime.Parse("2004-02-12"), DateCreated=DateTime.Now, DateModified=DateTime.Now}
            };
            foreach (Instructor instructor in instructors)
            {
                context.Instructors.Add(instructor);
            }
            context.SaveChanges();


            if (context.Departments.Any())
            {
                return;
            }

            var departments = new Department[]
            {
                new Department { DepartmentName="English", Budget=350000, StartDate=DateTime.Parse("2007-09-01"), InstructorID=instructors.Single( i => i.LastName == "Abercrombie").InstructorID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Department { DepartmentName="Mathematics", Budget=100000, StartDate=DateTime.Parse("2007-09-01"), InstructorID=instructors.Single( i => i.LastName == "Fakhouri").InstructorID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Department { DepartmentName="Engineering", Budget=350000, StartDate=DateTime.Parse("2007-09-01"), InstructorID=instructors.Single( i => i.LastName == "Harui").InstructorID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Department { DepartmentName="Economics", Budget=1000000, StartDate=DateTime.Parse("2007-09-01"), InstructorID=instructors.Single( i => i.LastName == "Kapoor").InstructorID, DateCreated=DateTime.Now, DateModified=DateTime.Now}
            };
            foreach (Department department in departments)
            {
                context.Departments.Add(department);
            }
            context.SaveChanges();

            if (context.Courses.Any())
            {
                return;
            }

            var courses = new Course[]
            {
                new Course {CourseId=1050, CourseName="Certificate in Plumbing(Plumbing)", CourseCredit=3, DepartmentID = departments.Single( s => s.DepartmentName == "Engineering").DepartmentID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Course {CourseId=4022, CourseName="Certificate in Electrical Installation(Electrical Installation)", CourseCredit=5, DepartmentID = departments.Single( s => s.DepartmentName == "Economics").DepartmentID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Course {CourseId=4041, CourseName="Certificate in Computer Studies(Computer Studies)", CourseCredit=1, DepartmentID = departments.Single( s => s.DepartmentName == "Economics").DepartmentID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Course {CourseId=1045, CourseName="Certificate in Metal Work Technical Education(Metal Work Technical Education)", CourseCredit=5, DepartmentID = departments.Single( s => s.DepartmentName == "Mathematics").DepartmentID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Course {CourseId=3141, CourseName="Certificate in Motor Mechanics(Motor Mechanics)", CourseCredit=4, DepartmentID = departments.Single( s => s.DepartmentName == "Mathematics").DepartmentID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Course {CourseId=2021, CourseName="Certificate in Upholstery(Upholstery)", CourseCredit=4, DepartmentID = departments.Single( s => s.DepartmentName == "English").DepartmentID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Course {CourseId=2042, CourseName="International Computer Driving Licence ICDL()", CourseCredit=3, DepartmentID = departments.Single( s => s.DepartmentName == "English").DepartmentID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Course {CourseId=1110, CourseName="Certificate in Carpentry and Joinery ()", CourseCredit=5, DepartmentID = departments.Single( s => s.DepartmentName == "English").DepartmentID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Course {CourseId=7890, CourseName="Certificate in Auto Electrical Engineering()", CourseCredit=5, DepartmentID = departments.Single( s => s.DepartmentName == "English").DepartmentID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Course {CourseId=3200, CourseName="Certificate in Building Construction()", CourseCredit=5, DepartmentID = departments.Single( s => s.DepartmentName == "Economics").DepartmentID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Course {CourseId=0030, CourseName="Certificate in Agriculture()", CourseCredit=3, DepartmentID = departments.Single( s => s.DepartmentName == "Engineering").DepartmentID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Course {CourseId=6060, CourseName="Certificate in Panel Beating and Spray Painting ()", CourseCredit=1, DepartmentID = departments.Single( s => s.DepartmentName == "Engineering").DepartmentID, DateCreated=DateTime.Now, DateModified=DateTime.Now}
            };

            foreach (Course course in courses)
            {
                context.Courses.Add(course);
            }
            context.SaveChanges();

            if (context.OfficeAssignments.Any())
            {
                return;
            }

            var officeAssignments = new OfficeAssignment[]
            {
                new OfficeAssignment {InstructorID=instructors.Single( i => i.LastName == "Fakhouri").InstructorID, Location="Smith 17", DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new OfficeAssignment {InstructorID=instructors.Single( i => i.LastName == "Harui").InstructorID, Location="Gowan 27", DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new OfficeAssignment {InstructorID=instructors.Single( i => i.LastName == "Kapoor").InstructorID, Location="Thompson 304", DateCreated=DateTime.Now, DateModified=DateTime.Now}
            };

            foreach (OfficeAssignment officeAssignment in officeAssignments)
            {
                context.OfficeAssignments.Add(officeAssignment);
            }
            context.SaveChanges();

            if (context.CourseAssignments.Any())
            {
                return;
            }

            var courseInstructors = new CourseAssignment[]
            {
                new CourseAssignment {CourseId=courses.Single(c => c.CourseName == "Certificate in Plumbing(Plumbing)").CourseId, InstructorId=instructors.Single( i => i.LastName == "Kapoor").InstructorID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new CourseAssignment {CourseId=courses.Single(c => c.CourseName == "Certificate in Electrical Installation(Electrical Installation)").CourseId, InstructorId=instructors.Single( i => i.LastName == "Harui").InstructorID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new CourseAssignment {CourseId=courses.Single(c => c.CourseName == "Certificate in Computer Studies(Computer Studies)").CourseId, InstructorId=instructors.Single( i => i.LastName == "Zheng").InstructorID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new CourseAssignment {CourseId=courses.Single(c => c.CourseName == "Certificate in Metal Work Technical Education(Metal Work Technical Education)").CourseId, InstructorId=instructors.Single( i => i.LastName == "Zheng").InstructorID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new CourseAssignment {CourseId=courses.Single(c => c.CourseName == "Certificate in Motor Mechanics(Motor Mechanics)").CourseId, InstructorId=instructors.Single( i => i.LastName == "Fakhouri").InstructorID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new CourseAssignment {CourseId=courses.Single(c => c.CourseName == "Certificate in Upholstery(Upholstery)").CourseId, InstructorId=instructors.Single( i => i.LastName == "Fakhouri").InstructorID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new CourseAssignment {CourseId=courses.Single(c => c.CourseName == "International Computer Driving Licence ICDL()").CourseId, InstructorId=instructors.Single( i => i.LastName == "Harui").InstructorID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new CourseAssignment {CourseId=courses.Single(c => c.CourseName == "Certificate in Auto Electrical Engineering()").CourseId, InstructorId=instructors.Single( i => i.LastName == "Abercrombie").InstructorID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new CourseAssignment {CourseId=courses.Single(c => c.CourseName == "Certificate in Plumbing(Plumbing)").CourseId, InstructorId=instructors.Single( i => i.LastName == "Abercrombie").InstructorID, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new CourseAssignment {CourseId=courses.Single(c => c.CourseName == "Certificate in Metal Work Technical Education(Metal Work Technical Education)").CourseId, InstructorId=instructors.Single( i => i.LastName == "Abercrombie").InstructorID, DateCreated=DateTime.Now, DateModified=DateTime.Now}
            };

            foreach (CourseAssignment courseInstructor in courseInstructors)
            {
                context.CourseAssignments.Add(courseInstructor);
            }
            context.SaveChanges();


            if (context.Enrollments.Any())
            {
                return;
            }

            var enrollments = new Enrollment[]
            {
                new Enrollment {StudentID=1, CourseID=1050, Grade=GRADE.A, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=1, CourseID=6060, Grade=GRADE.B, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=1, CourseID=3200, Grade=GRADE.F, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=1, CourseID=7890, Grade=GRADE.A, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=2, CourseID=1050, Grade=GRADE.B, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=2, CourseID=6060, Grade=GRADE.B, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=3, CourseID=4022, Grade=GRADE.F, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=3, CourseID=7890, Grade=GRADE.A, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=3, CourseID=1050, Grade=GRADE.A, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=4, CourseID=6060, Grade=GRADE.B, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=4, CourseID=3200, Grade=GRADE.F, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=5, CourseID=7890, Grade=GRADE.A, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=6, CourseID=1050, Grade=GRADE.A, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=6, CourseID=6060, Grade=GRADE.B, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=7, CourseID=3200, Grade=GRADE.F, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=7, CourseID=7890, Grade=GRADE.A, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=8, CourseID=1050, Grade=GRADE.B, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=9, CourseID=2042, Grade=GRADE.B, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=9, CourseID=1110, Grade=GRADE.C, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=9, CourseID=7890, Grade=GRADE.NOGRADE, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=9, CourseID=1050, Grade=GRADE.A, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=9, CourseID=6060, Grade=GRADE.B, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=9, CourseID=3200, Grade=GRADE.C, DateCreated=DateTime.Now, DateModified=DateTime.Now},
                new Enrollment {StudentID=9, CourseID=4041, Grade=GRADE.A, DateCreated=DateTime.Now, DateModified=DateTime.Now}
            };

            foreach (Enrollment enrollment in enrollments)
            {
                context.Enrollments.Add(enrollment);
            }
            context.SaveChanges();
        }
    }
}
