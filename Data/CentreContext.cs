﻿using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models;
using Microsoft.EntityFrameworkCore;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Data
{
    public class CentreContext: DbContext
    {
        public CentreContext(DbContextOptions<CentreContext> options) : base(options)
        {

        }

        public DbSet<Course> Courses { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }

        public DbSet<Department> Departments { get; set; }
        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<OfficeAssignment> OfficeAssignments { get; set; }
        public DbSet<CourseAssignment> CourseAssignments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>().ToTable("Class");
            modelBuilder.Entity<Student>().ToTable("Student");
            modelBuilder.Entity<Enrollment>().ToTable("Enrollment");

            modelBuilder.Entity<Department>().ToTable("Department");
            modelBuilder.Entity<Instructor>().ToTable("Instructor");
            modelBuilder.Entity<OfficeAssignment>().ToTable("OfficeAssignment");
            modelBuilder.Entity<CourseAssignment>().ToTable("CourseAssignment");

            // playing with configuration: composite keys
            modelBuilder.Entity<CourseAssignment>().HasKey(c => new
            {
                c.CourseId,
                c.InstructorId
            }); 
        }
    }
}
