﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Data;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Controllers
{
    public class OfficeAssignmentsController : Controller
    {
        private readonly CentreContext _context;

        public OfficeAssignmentsController(CentreContext context)
        {
            _context = context;
        }

        // GET: OfficeAssignments
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["LocationParm"] = String.IsNullOrEmpty(sortOrder) ? "location_desc" : "";
            ViewData["InstructorNameParm"] = sortOrder == "InstructorName" ? "instructor_name_desc" : "InstructorName";
            
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;

            var officeassignments = from oa in _context.OfficeAssignments
                          select oa;

            officeassignments = officeassignments
                .Include(oa => oa.Instructor);

            if (!String.IsNullOrEmpty(searchString))
            {
                officeassignments = officeassignments.Where(oa => oa.Location.Contains(searchString) || oa.Instructor.LastName.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "location_desc":
                    officeassignments = officeassignments.OrderByDescending(oa => oa.Location);
                    break;
                case "InstructorName":
                    officeassignments = officeassignments.OrderBy(oa => oa.Instructor.LastName);
                    break;
                case "instructor_name_desc":
                    officeassignments = officeassignments.OrderByDescending(oa => oa.Instructor.LastName);
                    break;
                default:
                    officeassignments = officeassignments.OrderBy(oa => oa.Location);
                    break;
            }

            int pageSize = 8;
            return View(await PaginatedList<OfficeAssignment>.CreateAsync(officeassignments.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: OfficeAssignments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var officeAssignment = await _context.OfficeAssignments
                .Include(o => o.Instructor)
                .FirstOrDefaultAsync(m => m.InstructorID == id);
            if (officeAssignment == null)
            {
                return NotFound();
            }

            return View(officeAssignment);
        }

        // GET: OfficeAssignments/Create
        public IActionResult Create()
        {
            PopulateInstructorsDropDownList();
            return View();
        }

        // POST: OfficeAssignments/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("InstructorID,Location")] OfficeAssignment officeAssignment)
        {
            if (ModelState.IsValid)
            {
                officeAssignment.DateCreated = DateTime.Now;
                officeAssignment.DateModified = DateTime.Now;
                _context.Add(officeAssignment);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["InstructorID"] = new SelectList(_context.Instructors, "InstructorID", "FirstName", officeAssignment.InstructorID);
            return View(officeAssignment);
        }

        // GET: OfficeAssignments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var officeAssignment = await _context.OfficeAssignments.FindAsync(id);
            if (officeAssignment == null)
            {
                return NotFound();
            }
            PopulateInstructorsDropDownList(officeAssignment.InstructorID);
            return View(officeAssignment);
        }

        // POST: OfficeAssignments/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("InstructorID,Location")] OfficeAssignment officeAssignment)
        {
            if (id != officeAssignment.InstructorID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    officeAssignment.DateModified = DateTime.Now;
                    _context.Update(officeAssignment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OfficeAssignmentExists(officeAssignment.InstructorID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            PopulateInstructorsDropDownList(officeAssignment.InstructorID);
            return View(officeAssignment);
        }

        // GET: OfficeAssignments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var officeAssignment = await _context.OfficeAssignments
                .Include(o => o.Instructor)
                .FirstOrDefaultAsync(m => m.InstructorID == id);
            if (officeAssignment == null)
            {
                return NotFound();
            }

            return View(officeAssignment);
        }

        // POST: OfficeAssignments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var officeAssignment = await _context.OfficeAssignments.FindAsync(id);
            _context.OfficeAssignments.Remove(officeAssignment);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OfficeAssignmentExists(int id)
        {
            return _context.OfficeAssignments.Any(e => e.InstructorID == id);
        }

        private void PopulateInstructorsDropDownList(object selectedInstructor = null)
        {
            var instructorsQuery = from i in _context.Instructors
                                   orderby i.LastName
                                   select i;
            ViewData["InstructorID"] = new SelectList(instructorsQuery.AsNoTracking(), "InstructorID", "FullName", selectedInstructor);
        }
    }
}
