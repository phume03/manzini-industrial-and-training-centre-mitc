﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Data;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models.SchoolViewModels;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Controllers
{
    public class StudentsController : Controller
    {
        private readonly CentreContext _context;

        public StudentsController(CentreContext context)
        {
            _context = context;
        }

        // GET: Students
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["LNameSortParm"] = sortOrder == "LastName" ? "lname_desc" : "LastName";
            ViewData["DateEnrolledSortParm"] = sortOrder == "DateEnrolled" ? "date_en_desc" : "DateEnrolled";
            ViewData["DateCreatedSortParm"] = sortOrder == "DateCreated" ? "date_cr_desc" : "DateCreated";
            ViewData["DateModifiedSortParm"] = sortOrder == "DateModified" ? "date_mod_desc" : "DateModified";

            if (searchString != null)
            {
                pageNumber = 1;
            } else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;

            var students = from s in _context.Students
                           select s;

            if (!String.IsNullOrEmpty(searchString))
            {
                students = students.Where(s => s.StudentLastName.Contains(searchString) || s.StudentFirstName.Contains(searchString));
            }

            switch(sortOrder)
            {
                case "name_desc":
                    students = students.OrderByDescending(s => s.StudentFirstName);
                    break;
                case "LastName":
                    students = students.OrderBy(s => s.StudentLastName);
                    break;
                case "lname_desc":
                    students = students.OrderByDescending(s => s.StudentLastName);
                    break;
                case "DateEnrolled":
                    students = students.OrderBy(s => s.EnrollmentDate);
                    break;
                case "date_en_desc":
                    students = students.OrderByDescending(s => s.EnrollmentDate);
                    break;
                case "DateCreated":
                    students = students.OrderBy(s => s.DateCreated);
                    break;
                case "date_cr_desc":
                    students = students.OrderByDescending(s => s.DateCreated);
                    break;
                case "DateModified":
                    students = students.OrderBy(s => s.DateModified);
                    break;
                case "date_mod_desc":
                    students = students.OrderByDescending(s => s.DateModified);
                    break;
                default:
                    students = students.OrderBy(s => s.StudentFirstName);
                    break;
            }

            int pageSize = 8;
            return View(await PaginatedList<Student>.CreateAsync(students.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: Students/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students
                .Include(s => s.Enrollments)
                .ThenInclude(e => e.Course)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.StudentId == id);

            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // GET: Students/Create
        public IActionResult Create()
        {
            var student = new Student();
            student.Enrollments = new List<Enrollment>();
            PopulateEnrolledCourseData(student);
            return View();
        }

        // POST: Students/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StudentFirstName,StudentLastName,EnrollmentDate")] Student student, string[] selectedCourses)
        {
            if (selectedCourses != null)
            {
                student.Enrollments = new List<Enrollment>();
                foreach (var course in selectedCourses)
                {
                    var courseToAdd = new Enrollment
                    {
                        CourseID = int.Parse(course),
                        StudentID = student.StudentId,
                        Grade = GRADE.NOGRADE,
                        DateCreated = DateTime.Now,
                        DateModified = DateTime.Now
                    };
                    student.Enrollments.Add(courseToAdd);
                }
            }

            try
            {
                if (ModelState.IsValid)
                {
                    student.DateCreated = DateTime.Now;
                    student.DateModified = DateTime.Now;
                    _context.Add(student);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                PopulateEnrolledCourseData(student);
            } catch (DbUpdateException)
            {
                ModelState.AddModelError("", @$"Unable to save changes... 
                    Try again, and if the problem persists see your system administrator.");
            }
            
            return View(student);
        }

        // GET: Students/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students
                .Include(s => s.Enrollments)
                    .ThenInclude(e => e.Course)
                .AsNoTracking()
                .FirstOrDefaultAsync(s => s.StudentId == id);

            if (student == null || id != student.StudentId)
            {
                return NotFound();
            }
            PopulateEnrolledCourseData(student);
            return View(student);
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, string[] selectedCourses)
        {
            if (id.GetType() != typeof(int))
            {
                return NotFound();
            }

            var studentToUpdate = await _context.Students
                .Include(s => s.Enrollments)
                    .ThenInclude(e => e.Course)
                .AsNoTracking()
                .FirstOrDefaultAsync(s => s.StudentId == id);

            if (studentToUpdate == null || studentToUpdate.StudentId != id)
            {
                return NotFound();
            }
            if (await TryUpdateModelAsync<Student>(studentToUpdate, "", s => s.StudentFirstName, s => s.StudentLastName, s => s.EnrollmentDate)) {
                UpdateStudentCourses(selectedCourses, studentToUpdate);
                try
                {
                    studentToUpdate.DateModified = DateTime.Now;
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    if (!StudentExists(studentToUpdate.StudentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, please see your system administrator.");
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            UpdateStudentCourses(selectedCourses, studentToUpdate);
            PopulateEnrolledCourseData(studentToUpdate);
            return View(studentToUpdate);
        }

        // GET: Students/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students
                .Include(s => s.Enrollments)
                .ThenInclude(e => e.Course)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.StudentId == id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            Student student = await _context.Students
                .Include(i => i.Enrollments)
                .SingleOrDefaultAsync(s => s.StudentId == id);

            if (student == null)
            {
                return RedirectToAction(nameof(Index));
            }

            try
            {
                _context.Students.Remove(student);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            } catch (DbUpdateException)
            {
                return RedirectToAction(nameof(Delete), new {id=id});
            }
            
        }

        private bool StudentExists(int id)
        {
            return _context.Students.Any(e => e.StudentId == id);
        }
    
        private void PopulateEnrolledCourseData(Student student)
        {
            var allCourses = _context.Courses;
            var studentCourses = new HashSet<int>(student.Enrollments
                .Select(c => c.CourseID));
            var viewModel = new List<EnrolledCourseData>();
            foreach(var course in allCourses)
            {
                viewModel.Add(new EnrolledCourseData
                {
                    CourseID = course.CourseId,
                    CourseName = course.CourseName,
                    Enrolled = studentCourses.Contains(course.CourseId)
                });
            }
            ViewData["Courses"] = viewModel;
        }
    
        private void UpdateStudentCourses(string [] selectedCourses, Student student)
        {
            if (selectedCourses == null)
            {
                student.Enrollments = new List<Enrollment>();
                return;
            }

            var selectedCoursesHS = new HashSet<string>(selectedCourses);
            var studentCourses = new HashSet<int>(
                student.Enrollments.Select(
                    c => c.Course.CourseId
                )
            );

            var courses = from c in _context.Courses
                          select c;
            foreach (Course generalCourse in courses)
            {
                // selected add
                if (selectedCoursesHS.Contains(generalCourse.CourseId.ToString()))
                {
                    if (!studentCourses.Contains(generalCourse.CourseId))
                    {
                        Enrollment courseToAdd = new Enrollment
                        {
                            StudentID = student.StudentId,
                            CourseID = generalCourse.CourseId,
                            Grade = GRADE.NOGRADE
                        };
                        student.Enrollments.Add(courseToAdd);
                    }
                } else
                {
                    // not selected remove
                    if (studentCourses.Contains(generalCourse.CourseId))
                    {
                        Enrollment courseToRemove = student.Enrollments
                            .FirstOrDefault(s => s.CourseID == generalCourse.CourseId);
                        _context.Remove(courseToRemove);
                    }
                }
            }
            return;
        }
    }
}
