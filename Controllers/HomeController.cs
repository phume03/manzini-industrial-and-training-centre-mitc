﻿using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models.SchoolViewModels;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly CentreContext _context;

        public HomeController(ILogger<HomeController> logger, CentreContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> About(string sortOrder, string currentFilter, int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["EnrollmentDateParm"] = String.IsNullOrEmpty(sortOrder) ? "enrollment_date_desc" : "";
            ViewData["EnrollmentCountParm"] = sortOrder == "EnrollmentCount" ? "enrollment_count_desc" : "EnrollmentCount";
            
            IQueryable<EnrollmentDateGroup> data =
                from student in _context.Students
                group student by student.EnrollmentDate into dateGroup
                select new EnrollmentDateGroup()
                {
                    EnrollmentDate = dateGroup.Key,
                    StudentCount = dateGroup.Count()
                };

            switch (sortOrder)
            {
                case "enrollment_date_desc":
                    data = data.OrderByDescending(dat => dat.EnrollmentDate);
                    break;
                case "EnrollmentCount":
                    data = data.OrderBy(dat => dat.StudentCount);
                    break;
                case "enrollment_count_desc":
                    data = data.OrderByDescending(dat => dat.StudentCount);
                    break;
                default:
                    data = data.OrderBy(dat => dat.EnrollmentDate);
                    break;
            }
            int pageSize = 8;
            return View(await PaginatedList<EnrollmentDateGroup>.CreateAsync(data.AsNoTracking(), pageNumber ?? 1, pageSize));            
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
