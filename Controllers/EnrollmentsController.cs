﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Data;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Controllers
{
    public class EnrollmentsController : Controller
    {
        private readonly CentreContext _context;

        public EnrollmentsController(CentreContext context)
        {
            _context = context;
        }

        // GET: Enrollments
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["GradeSortParm"] = String.IsNullOrEmpty(sortOrder) ? "course_grade_desc" : "";
            ViewData["NumberSortParm"] = sortOrder == "CourseNumber" ? "course_num_desc" : "CourseNumber";
            ViewData["NameSortParm"] = sortOrder == "StudentName" ? "student_name_desc" : "StudentName";
            ViewData["DateCreatedSortParm"] = sortOrder == "DateCreated" ? "date_cr_desc" : "DateCreated";
            ViewData["DateModifiedSortParm"] = sortOrder == "DateModified" ? "date_mod_desc" : "DateModified";
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;


            var enrollments = from e in _context.Enrollments
                         select e;
            enrollments = enrollments
                .Include(e => e.Course)
                .Include(e => e.Student);

            if (!String.IsNullOrEmpty(searchString))
            {
                enrollments = enrollments.Where(e => e.CourseID.Equals(searchString) || e.StudentID.Equals(searchString));
            }

            switch (sortOrder)
            {
                case "course_grade_desc":
                    enrollments = enrollments.OrderByDescending(e => e.Grade);
                    break;
                case "CourseNumber":
                    enrollments = enrollments.OrderBy(e => e.CourseID);
                    break;
                case "course_num_desc":
                    enrollments = enrollments.OrderByDescending(e => e.CourseID);
                    break;
                case "StudentName":
                    enrollments = enrollments.OrderBy(e => e.StudentID);
                    break;
                case "student_name_desc":
                    enrollments = enrollments.OrderByDescending(e => e.StudentID);
                    break;
                case "DateCreated":
                    enrollments = enrollments.OrderBy(e => e.DateCreated);
                    break;
                case "date_cr_desc":
                    enrollments = enrollments.OrderByDescending(e => e.DateCreated);
                    break;
                case "DateModified":
                    enrollments = enrollments.OrderBy(e => e.DateModified);
                    break;
                case "date_mod_desc":
                    enrollments = enrollments.OrderByDescending(e => e.DateModified);
                    break;
                default:
                    enrollments = enrollments.OrderBy(e => e.Grade);
                    break;
            }
            int pageSize = 8;
            return View(await PaginatedList<Enrollment>.CreateAsync(enrollments.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: Enrollments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enrollment = await _context.Enrollments
                .Include(e => e.Course)
                .Include(e => e.Student)
                .FirstOrDefaultAsync(m => m.EnrollmentID == id);
            if (enrollment == null)
            {
                return NotFound();
            }

            return View(enrollment);
        }

        // GET: Enrollments/Create
        public IActionResult Create()
        {
            ViewData["CourseID"] = new SelectList(_context.Courses, "CourseId", "CourseId");
            ViewData["StudentID"] = new SelectList(_context.Students, "StudentId", "StudentId");
            string [] grades = Enum.GetNames<GRADE>();
            ViewData["Grades"] = new SelectList(grades);
            return View();
        }

        // POST: Enrollments/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EnrollmentID,CourseID,StudentID,Grade")] Enrollment enrollment)
        {
            enrollment.DateCreated = DateTime.Now;
            enrollment.DateModified = DateTime.Now;
            if (ModelState.IsValid)
            {
                _context.Add(enrollment);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CourseID"] = new SelectList(_context.Courses, "CourseId", "CourseId", enrollment.CourseID);
            ViewData["StudentID"] = new SelectList(_context.Students, "StudentId", "StudentId", enrollment.StudentID);
            return View(enrollment);
        }

        // GET: Enrollments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enrollment = await _context.Enrollments.FindAsync(id);
            if (enrollment == null)
            {
                return NotFound();
            }
            ViewData["CourseID"] = new SelectList(_context.Courses, "CourseId", "CourseId", enrollment.CourseID);
            ViewData["StudentID"] = new SelectList(_context.Students, "StudentId", "StudentId", enrollment.StudentID);
            string[] grades = Enum.GetNames<GRADE>();
            ViewData["Grades"] = new SelectList(grades);
            return View(enrollment);
        }

        // POST: Enrollments/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id)
        {
            if (id.GetType() != typeof(int))
            {
                return NotFound();
            }

            var enrollment = await _context.Enrollments.FindAsync(id);
            if (enrollment == null || id != enrollment.EnrollmentID)
            {
                return NotFound();
            }

            if (await TryUpdateModelAsync<Enrollment>(enrollment, "", e => e.Grade))
            {
                try
                {
                    enrollment.DateModified = DateTime.Now;
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EnrollmentExists(enrollment.EnrollmentID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CourseID"] = new SelectList(_context.Courses, "CourseId", "CourseId", enrollment.CourseID);
            ViewData["StudentID"] = new SelectList(_context.Students, "StudentId", "StudentId", enrollment.StudentID);
            return View(enrollment);
        }

        // GET: Enrollments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enrollment = await _context.Enrollments
                .Include(e => e.Course)
                .Include(e => e.Student)
                .FirstOrDefaultAsync(m => m.EnrollmentID == id);
            if (enrollment == null)
            {
                return NotFound();
            }

            return View(enrollment);
        }

        // POST: Enrollments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var enrollment = await _context.Enrollments.FindAsync(id);
            if (enrollment == null)
            {
                return RedirectToAction(nameof(Index));
            }

            try
            {
                _context.Enrollments.Remove(enrollment);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {
                return RedirectToAction(nameof(Delete), new { id = id });
            }
        }

        private bool EnrollmentExists(int id)
        {
            return _context.Enrollments.Any(e => e.EnrollmentID == id);
        }
    }
}
