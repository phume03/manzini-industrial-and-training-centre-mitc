﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Data;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Controllers
{
    public class DepartmentsController : Controller
    {
        private readonly CentreContext _context;

        public DepartmentsController(CentreContext context)
        {
            _context = context;
        }

        // GET: Departments
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["DepartmentNameParm"] = String.IsNullOrEmpty(sortOrder) ? "department_name_desc" : "";
            ViewData["BudgetParm"] = sortOrder == "BudgetValue" ? "budget_value_desc" : "BudgetValue";
            ViewData["StartDateParm"] = sortOrder == "StartDate" ? "start_date_desc" : "StartDate";
            ViewData["InstructorNameParm"] = sortOrder == "InstructorName" ? "instructor_name_desc" : "InstructorName";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;

            var departments = from d in _context.Departments
                              select d;
            departments = departments
                .Include(d => d.Administrator);

            if (!String.IsNullOrEmpty(searchString))
            {
                departments = departments.Where(d => d.DepartmentName.Contains(searchString) || d.Budget.Equals(searchString));
            }

            switch (sortOrder)
            {
                case "department_name_desc":
                    departments = departments.OrderByDescending(d => d.DepartmentName);
                    break;
                case "BudgetValue":
                    departments = departments.OrderBy(d => d.Budget);
                    break;
                case "budget_value_desc":
                    departments = departments.OrderByDescending(d => d.Budget);
                    break;
                case "StartDate":
                    departments = departments.OrderBy(d => d.StartDate);
                    break;
                case "start_date_desc":
                    departments = departments.OrderByDescending(d => d.StartDate);
                    break;
                case "InstructorName":
                    departments = departments.OrderBy(d => d.Administrator.LastName);
                    break;
                case "instructor_name_desc":
                    departments = departments.OrderByDescending(d => d.Administrator.LastName);
                    break;
                default:
                    departments = departments.OrderBy(d => d.DepartmentName);
                    break;
            }

            int pageSize = 8;
            return View(await PaginatedList<Department>.CreateAsync(departments.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: Departments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var department = await _context.Departments
                .Include(d => d.Administrator)
                .FirstOrDefaultAsync(m => m.DepartmentID == id);
            if (department == null)
            {
                return NotFound();
            }

            return View(department);
        }

        // GET: Departments/Create
        public IActionResult Create()
        {
            PopulateInstructorData();
            return View();
        }

        // POST: Departments/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DepartmentID,DepartmentName,Budget,StartDate,InstructorID")] Department department)
        {
            department.DateCreated = DateTime.Now;
            department.DateModified = DateTime.Now;
            if (ModelState.IsValid)
            {
                _context.Add(department);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            PopulateInstructorData(department.InstructorID);
            return View(department);
        }

        // GET: Departments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var department = await _context.Departments.FindAsync(id);
            if (department == null)
            {
                return NotFound();
            }
            PopulateInstructorData(department.InstructorID);
            return View(department);
        }

        // POST: Departments/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DepartmentID,DepartmentName,Budget,StartDate,InstructorID,DateCreated,DateModified,DateDeleted")] Department department)
        {
            if (id != department.DepartmentID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    department.DateModified = DateTime.Now;
                    _context.Update(department);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DepartmentExists(department.DepartmentID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            PopulateInstructorData(department.InstructorID);
            return View(department);
        }

        // GET: Departments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var department = await _context.Departments
                .Include(d => d.Administrator)
                .FirstOrDefaultAsync(m => m.DepartmentID == id);
            if (department == null)
            {
                return NotFound();
            }

            return View(department);
        }

        // POST: Departments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var department = await _context.Departments.FindAsync(id);
            _context.Departments.Remove(department);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DepartmentExists(int id)
        {
            return _context.Departments.Any(e => e.DepartmentID == id);
        }
    
        private void PopulateInstructorData(object department = null)
        {
            ViewData["InstructorID"] = new SelectList(_context.Instructors, "InstructorID", "FullName", department);
        }
    }
}
