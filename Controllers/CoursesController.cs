﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Data;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Controllers
{
    public class CoursesController : Controller
    {
        private readonly CentreContext _context;

        public CoursesController(CentreContext context)
        {
            _context = context;
        }

        // GET: Courses
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NumberSortParm"] = String.IsNullOrEmpty(sortOrder) ? "course_num_desc" : "";
            ViewData["NameSortParm"] = sortOrder == "CourseName" ? "course_name_desc" : "CourseName";
            ViewData["CreditSortParm"] = sortOrder == "CourseCredit" ? "course_credit_desc" : "CourseCredit";
            ViewData["DepartmentSortParm"] = sortOrder == "DepartmentName" ? "department_name_desc" : "DepartmentName";
            
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;

            var courses = from c in _context.Courses
                         select c;

            courses = courses.Include(d => d.Department);

            if (!String.IsNullOrEmpty(searchString))
            {
                courses = courses.Where(c => c.CourseName.Contains(searchString) || c.CourseId.Equals(searchString));
            }

            switch (sortOrder)
            {
                case "course_num_desc":
                    courses = courses.OrderByDescending(c => c.CourseId);
                    break;
                case "CourseName":
                    courses = courses.OrderBy(c => c.CourseName);
                    break;
                case "course_name_desc":
                    courses = courses.OrderByDescending(c => c.CourseName);
                    break;
                case "CourseCredit":
                    courses = courses.OrderBy(c => c.CourseCredit);
                    break;
                case "course_credit_desc":
                    courses = courses.OrderByDescending(c => c.CourseCredit);
                    break;
                case "DepartmentName":
                    courses = courses.OrderBy(c => c.Department.DepartmentName);
                    break;
                case "department_name_desc":
                    courses = courses.OrderByDescending(c => c.Department.DepartmentName);
                    break;
                case "DateCreated":
                    courses = courses.OrderBy(c => c.DateCreated);
                    break;
                case "date_cr_desc":
                    courses = courses.OrderByDescending(c => c.DateCreated);
                    break;
                case "DateModified":
                    courses = courses.OrderBy(c => c.DateModified);
                    break;
                case "date_mod_desc":
                    courses = courses.OrderByDescending(c => c.DateModified);
                    break;
                default:
                    courses = courses.OrderBy(c => c.CourseId);
                    break;
            }

            int pageSize = 8;
            return View(await PaginatedList<Course>.CreateAsync(courses.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: Courses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var course = await _context.Courses
                .Include(c => c.Department)
                .Include(c => c.Enrollments)
                    .ThenInclude(e => e.Student)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.CourseId == id);

            if (course == null)
            {
                return NotFound();
            }

            return View(course);
        }

        public IActionResult Create()
        {
            PopulateDepartmentsDropDownList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CourseID, Credits, DepartmentID, Title")] Course course)
        {
            if (ModelState.IsValid)
            {
                course.DateCreated = DateTime.Now;
                course.DateModified = DateTime.Now;
                _context.Add(course);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            PopulateDepartmentsDropDownList(course.DepartmentID);
            return View(course);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var course = await _context.Courses
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.CourseId == id);
            if (course == null)
            {
                return NotFound();
            }
            PopulateDepartmentsDropDownList(course.DepartmentID);
            return View(course);
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var courseToUpdate = await _context.Courses
                .FirstOrDefaultAsync(c => c.CourseId == id);
            courseToUpdate.DateModified = DateTime.Now;

            if (await TryUpdateModelAsync<Course>(courseToUpdate, "", c => c.CourseCredit, c => c.DepartmentID, c => c.CourseName))
            {
                try
                {
                    await _context.SaveChangesAsync();
                } catch (DbUpdateException /* ex */)
                {
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
                return RedirectToAction(nameof(Index));
            }
            PopulateDepartmentsDropDownList(courseToUpdate.DepartmentID);
            return View(courseToUpdate);
        }

        private void PopulateDepartmentsDropDownList(object selectedDepartment = null)
        {
            var departmentsQuery = from d in _context.Departments
                                   orderby d.DepartmentName
                                   select d;
            ViewBag.DepartmentID = new SelectList(departmentsQuery.AsNoTracking(), "DepartmentID", "DepartmentName", selectedDepartment);
        }

        // GET: Courses/Create
        /*public IActionResult Create()
        {
            return View();
        }*/

        // POST: Courses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /*[HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CourseId,CourseName,CourseCredit")] Course course)
        {
            course.DateCreated = DateTime.Now;
            course.DateModified = DateTime.Now;
            if (ModelState.IsValid)
            {
                _context.Add(course);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(course);
        }*/

        // GET: Courses/Edit/5
        /*public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var course = await _context.Courses.FindAsync(id);
            if (course == null)
            {
                return NotFound();
            }
            return View(course);
        }*/

        // POST: Courses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /*[HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id)
        {
            if (id.GetType() != typeof(int))
            {
                return NotFound();
            }

            var course = await _context.Courses.FindAsync(id);
            if (course == null || id != course.CourseId)
            {
                return NotFound();
            }

            if (await TryUpdateModelAsync<Course>(course, "", c => c.CourseId, c => c.CourseName, c => c.CourseCredit))
            {
                try
                {
                    course.DateModified = DateTime.Now;
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CourseExists(course.CourseId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(course);
        }*/

        // GET: Courses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var course = await _context.Courses
                .Include(c => c.Department)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.CourseId == id);

            if (course == null)
            {
                return NotFound();
            }

            return View(course);
        }

        // POST: Courses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var course = await _context.Courses.FindAsync(id);
            if (course == null)
            {
                return RedirectToAction(nameof(Index));
            }

            try
            {
                _context.Courses.Remove(course);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {
                return RedirectToAction(nameof(Delete), new { id = id });
            }
        }

        private bool CourseExists(int id)
        {
            return _context.Courses.Any(e => e.CourseId == id);
        }
    }
}
