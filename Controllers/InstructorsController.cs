﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Data;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models.SchoolViewModels;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Controllers
{
    public class InstructorsController : Controller
    {
        private readonly CentreContext _context;

        public InstructorsController(CentreContext context)
        {
            _context = context;
        }

        // GET: Instructors
        public async Task<IActionResult> Index(int? id, int? courseID, string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["InstructorParm"] = String.IsNullOrEmpty(sortOrder) ? "instructor_name_desc" : "";
            ViewData["InstructorFirstNameParm"] = sortOrder == "FirstName" ? "instructor_first_name_desc" : "FirstName";
            ViewData["OfficeAssignmentSortParm"] = sortOrder == "OfficeAssignment" ? "office_assignment_desc" : "OfficeAssignment";
            ViewData["DateSortParm"] = sortOrder == "HireDate" ? "hire_date_desc" : "HireDate";
            ViewData["InstructorID"] = id;
            ViewData["CourseID"] = courseID;

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;

            var viewModel = new InstructorIndexData();

            var instructors = from i in _context.Instructors
                                    select i;
            instructors = instructors
                .Include(i => i.OfficeAssignment)
                .Include(i => i.CourseAssignments)
                    .ThenInclude(i => i.Course)
                    .ThenInclude(i => i.Enrollments)
                    .ThenInclude(i => i.Student)
                .Include(i => i.CourseAssignments)
                    .ThenInclude(i => i.Course)
                    .ThenInclude(i => i.Department)
                .AsNoTracking()
                .OrderBy(i => i.LastName);


            if (id != null)
            {
                ViewData["InstructorID"] = id.Value;
                Instructor instructor = instructors
                    .Where(i => i.InstructorID == id.Value).SingleOrDefault();
                viewModel.Courses = instructor.CourseAssignments.Select(s => s.Course);
            }

            if (courseID != null)
            {
                ViewData["CourseID"] = courseID.Value;
                viewModel.Enrollments = viewModel.Courses
                    .Where(x => x.CourseId == courseID).SingleOrDefault().Enrollments;
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                instructors = instructors.Where(vm => vm.FirstName.Contains(searchString) || vm.LastName.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "instructor_name_desc":
                    instructors = instructors.OrderByDescending(vm => vm.LastName);
                    break;
                case "FirstName":
                    instructors = instructors.OrderBy(vm => vm.FirstName);
                    break;
                case "instructor_first_name_desc":
                    instructors = instructors.OrderByDescending(vm => vm.FirstName);
                    break;
                case "OfficeAssignment":
                    instructors = instructors
                        .OrderBy(vm => vm.OfficeAssignment != null ? vm.OfficeAssignment.Location : null);
                    break;
                case "office_assignment_desc":
                    instructors = instructors.OrderByDescending(vm => vm.OfficeAssignment != null ? vm.OfficeAssignment.Location : null);
                    break;
                case "HireDate":
                    instructors = instructors.OrderBy(vm => vm.HireDate);
                    break;
                case "hire_date_desc":
                    instructors = instructors.OrderByDescending(vm => vm.HireDate);
                    break;
                default:
                    instructors = instructors.OrderBy(vm => vm.LastName);
                    break;
            }
            // return View(viewModel);
            int pageSize = 8;
            viewModel.Instructors = await PaginatedList<Instructor>.CreateAsync(instructors.AsNoTracking(), pageNumber ?? 1, pageSize);
            return View(viewModel);            
        }

        // GET: Instructors/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var instructor = await _context.Instructors
                .Include(i => i.CourseAssignments)
                    .ThenInclude(ca => ca.Course)
                    .ThenInclude(ca => ca.Department)
                .FirstOrDefaultAsync(m => m.InstructorID == id);
            if (instructor == null)
            {
                return NotFound();
            }

            return View(instructor);
        }

        // GET: Instructors/Create
        public IActionResult Create()
        {
            var instructor = new Instructor();
            instructor.CourseAssignments = new List<CourseAssignment>();
            PopulateOfficesDropDownList();
            PopulateAssignedCourseData(instructor);
            return View();
        }

        // POST: Instructors/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("InstructorID,LastName,FirstName,HireDate,OfficeAssignment")] Instructor instructor, string [] selectedCourses)
        {
            if (selectedCourses != null)
            {
                instructor.CourseAssignments = new List<CourseAssignment>(); 
                foreach(var course in selectedCourses)
                {
                    var courseToAdd = new CourseAssignment
                    {
                        DateCreated = DateTime.Now,
                        DateModified = DateTime.Now,
                        InstructorId = instructor.InstructorID,
                        CourseId = int.Parse(course)
                    };

                    instructor.CourseAssignments.Add(courseToAdd);
                }
            }

            if (ModelState.IsValid)
            {
                instructor.DateCreated = DateTime.Now;
                instructor.DateModified = DateTime.Now;
                _context.Add(instructor);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            PopulateOfficesDropDownList(instructor.InstructorID);
            PopulateAssignedCourseData(instructor);
            return View(instructor);
        }

        // GET: Instructors/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var instructor = await _context.Instructors
                .Include(i => i.OfficeAssignment)
                .Include(i => i.CourseAssignments)
                    .ThenInclude(i => i.Course)
                .AsNoTracking()
                .FirstOrDefaultAsync(i => i.InstructorID == id);
            
            if (instructor == null)
            {
                return NotFound();
            }
            PopulateAssignedCourseData(instructor);
            return View(instructor);
        }

        // POST: Instructors/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, string[] selectedCourses)
        {
            if (id == null)
            {
                return NotFound();
            }

            var instructorToUpdate = await _context.Instructors
                .Include(i => i.OfficeAssignment)
                .Include(i => i.CourseAssignments)
                    .ThenInclude(i => i.Course)
                .FirstOrDefaultAsync(i => i.InstructorID == id);
            instructorToUpdate.DateModified = DateTime.Now;

            if (await TryUpdateModelAsync<Instructor>(instructorToUpdate, "", i => i.LastName, i => i.FirstName, i => i.HireDate, i => i.OfficeAssignment))
            {
                if (String.IsNullOrEmpty(instructorToUpdate.OfficeAssignment?.Location))
                {
                    instructorToUpdate.OfficeAssignment = null;
                }
                UpdateInstructorCourses(selectedCourses, instructorToUpdate);

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateException /* ex */)
                {
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
                return RedirectToAction(nameof(Index));
            }
            UpdateInstructorCourses(selectedCourses, instructorToUpdate); // allows for a partial save of user data -- not yet persisted to the database, rather than losing changes
            PopulateAssignedCourseData(instructorToUpdate);
            return View(instructorToUpdate);
        }

        // GET: Instructors/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var instructor = await _context.Instructors
                .FirstOrDefaultAsync(m => m.InstructorID == id);
            if (instructor == null)
            {
                return NotFound();
            }

            return View(instructor);
        }

        // POST: Instructors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            Instructor instructor = await _context.Instructors
                .Include(i => i.CourseAssignments)
                .SingleOrDefaultAsync(i => i.InstructorID == id);

            var departments = await _context.Departments
                .Where(d => d.InstructorID == id)
                .ToListAsync();
            departments.ForEach(d => d.InstructorID = null);
            _context.Instructors.Remove(instructor);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InstructorExists(int id)
        {
            return _context.Instructors.Any(e => e.InstructorID == id);
        }

        private void PopulateOfficesDropDownList(object selectedOffice = null)
        {
            var officesQuery = from o in _context.OfficeAssignments
                                   orderby o.Location
                                   select o;
            ViewBag.InstructorID = new SelectList(officesQuery.AsNoTracking(), "InstructorID", "Location", selectedOffice);
        }

        private void PopulateAssignedCourseData(Instructor instructor)
        {
            var allCourses = _context.Courses;
            var instructorCourses = new HashSet<int>(instructor.CourseAssignments
                .Select(c => c.CourseId));
            var viewModel = new List<AssignedCourseData>();
            foreach (var course in allCourses)
            {
                viewModel.Add(new AssignedCourseData
                {
                    CourseID = course.CourseId,
                    CourseName = course.CourseName,
                    Assigned = instructorCourses.Contains(course.CourseId)
                });
            }
            ViewData["Courses"] = viewModel;
        }
    
        private void UpdateInstructorCourses(string[] selectedCourses, Instructor instructorToUpdate)
        {
            // Since the whole courses db is consulted, for every system actor that updates courses this operation slows the application significantly -- 
            if (selectedCourses == null)
            {
                instructorToUpdate.CourseAssignments = new List<CourseAssignment>();
                return;
            }

            var selectedCoursesHS = new HashSet<String>(selectedCourses);
            var instructorCourses = new HashSet<int>(instructorToUpdate.CourseAssignments
                .Select(c => c.Course.CourseId));

            foreach(var generalCourse in _context.Courses)
            {
                if (selectedCoursesHS.Contains(generalCourse.CourseId.ToString()))
                {
                    if (!instructorCourses.Contains(generalCourse.CourseId))
                    {
                        instructorToUpdate.CourseAssignments.Add(new CourseAssignment
                        {
                            InstructorId = instructorToUpdate.InstructorID,
                            CourseId = generalCourse.CourseId,
                            DateCreated = DateTime.Now,
                            DateModified = DateTime.Now
                        });
                    } else
                    {
                        // Do nothing, it was already added
                    }
                } else
                {
                    if (instructorCourses.Contains(generalCourse.CourseId))
                    {
                        CourseAssignment courseToRemove = instructorToUpdate.CourseAssignments
                            .FirstOrDefault(assign => assign.CourseId == generalCourse.CourseId);
                        _context.Remove(courseToRemove);
                    }
                }
            }
        }
    }
}
