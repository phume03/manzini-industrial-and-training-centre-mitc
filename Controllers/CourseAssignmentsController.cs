﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Data;
using Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Models;

namespace Manzini_Industrial_and_Training_Centre__MITC__Course_Scheduler.Controllers
{
    public class CourseAssignmentsController : Controller
    {
        private readonly CentreContext _context;

        public CourseAssignmentsController(CentreContext context)
        {
            _context = context;
        }

        // GET: CourseAssignments
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["CourseNameParm"] = String.IsNullOrEmpty(sortOrder) ? "course_name_desc" : "";
            ViewData["InstructorNameParm"] = sortOrder == "InstructorName" ? "instructor_name_desc" : "InstructorName";

            if (searchString != null)
            {
                pageNumber = 1;
            } else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;

            var courseassignments = from ca in _context.CourseAssignments
                select ca;
            courseassignments = courseassignments
                .Include(ca => ca.Course)
                .Include(ca => ca.Instructor);

            if (!String.IsNullOrEmpty(searchString))
            {
                courseassignments = courseassignments.Where(ca => ca.Course.CourseName.Contains(searchString) || ca.Instructor.FullName.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "course_name_desc":
                    courseassignments = courseassignments.OrderByDescending(ca => ca.Course.CourseName);
                    break;
                case "InstructorName":
                    courseassignments = courseassignments.OrderBy(ca => ca.Instructor.LastName);
                    break;
                case "instructor_name_desc":
                    courseassignments = courseassignments.OrderByDescending(ca => ca.Instructor.LastName);
                    break;
                default:
                    courseassignments = courseassignments.OrderBy(ca => ca.Course.CourseName);
                    break;
            }
            int pageSize = 8;
            /* var centreContext = _context.CourseAssignments.Include(c => c.Course).Include(c => c.Instructor); */
            return View(await PaginatedList <CourseAssignment>.CreateAsync(courseassignments.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: CourseAssignments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var courseAssignment = await _context.CourseAssignments
                .Include(c => c.Course)
                .Include(c => c.Instructor)
                .FirstOrDefaultAsync(m => m.CourseId == id);
            if (courseAssignment == null)
            {
                return NotFound();
            }

            return View(courseAssignment);
        }

        // GET: CourseAssignments/Create
        public IActionResult Create()
        {
            ViewData["CourseId"] = new SelectList(_context.Courses, "CourseId", "CourseId");
            ViewData["InstructorId"] = new SelectList(_context.Instructors, "InstructorID", "FirstName");
            return View();
        }

        // POST: CourseAssignments/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CourseId,InstructorId,DateCreated,DateModified,DateDeleted")] CourseAssignment courseAssignment)
        {
            if (ModelState.IsValid)
            {
                _context.Add(courseAssignment);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CourseId"] = new SelectList(_context.Courses, "CourseId", "CourseId", courseAssignment.CourseId);
            ViewData["InstructorId"] = new SelectList(_context.Instructors, "InstructorID", "FirstName", courseAssignment.InstructorId);
            return View(courseAssignment);
        }

        // GET: CourseAssignments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var courseAssignment = await _context.CourseAssignments.FindAsync(id);
            if (courseAssignment == null)
            {
                return NotFound();
            }
            ViewData["CourseId"] = new SelectList(_context.Courses, "CourseId", "CourseId", courseAssignment.CourseId);
            ViewData["InstructorId"] = new SelectList(_context.Instructors, "InstructorID", "FirstName", courseAssignment.InstructorId);
            return View(courseAssignment);
        }

        // POST: CourseAssignments/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CourseId,InstructorId,DateCreated,DateModified,DateDeleted")] CourseAssignment courseAssignment)
        {
            if (id != courseAssignment.CourseId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(courseAssignment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CourseAssignmentExists(courseAssignment.CourseId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CourseId"] = new SelectList(_context.Courses, "CourseId", "CourseId", courseAssignment.CourseId);
            ViewData["InstructorId"] = new SelectList(_context.Instructors, "InstructorID", "FirstName", courseAssignment.InstructorId);
            return View(courseAssignment);
        }

        // GET: CourseAssignments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var courseAssignment = await _context.CourseAssignments
                .Include(c => c.Course)
                .Include(c => c.Instructor)
                .FirstOrDefaultAsync(m => m.CourseId == id);
            if (courseAssignment == null)
            {
                return NotFound();
            }

            return View(courseAssignment);
        }

        // POST: CourseAssignments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var courseAssignment = await _context.CourseAssignments.FindAsync(id);
            _context.CourseAssignments.Remove(courseAssignment);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CourseAssignmentExists(int id)
        {
            return _context.CourseAssignments.Any(e => e.CourseId == id);
        }
    }
}
