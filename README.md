# Manzini Industrial and Training Centre (MITC) Course Scheduler #

This is an ASP .Net Core (.net5.0) Web App (using MVC framework). -- stopped on 1057

### Application Specifics ###

* ASP.Net Core 5.x
* Entity Framework (EF) Core 5.x



### Required Nuget Packages ###

```` Install-Package <PackageName> ````


* Microsoft.EntityFrameworkCore
* Microsoft.EntityFrameworkCore.Relational
* Microsoft.EntityFrameworkCore.Relational.Design
* Microsoft.AspNetCore.Diagnostics.EntityFrameworkCore
* Microsoft.EntityFrameworkCore.SqlServer
* Microsoft.EntityFrameworkCore.Tools
* Microsoft.EntityFrameworkCore.DbContext
* Microsoft.VisualStudio.Web.CodeGeneration.Design



## Development - Edit code: ##

### Read and update - TryUpdateModelAsync ###

After scaffolding, the ````[HttpPost]```` **Edit** (Update) action method is created with a ````Bind```` attribute. This attribute is then filled with the updated
data from user input using an instance of the entity containing the form data (forcing only one post operation). Any fields that are not updated are set to null! 
Then the new "bound" entity has an internal flag of "modified" that is set. After which, it is updated within the current context and the information is transferred 
to the database. Being that this is not always desirable, for example, if your form does not include some entity fields -- as the missing fields will be set to null.

An alternative approach to saving edits as well as a _security best practice_ is to do away with the bind attribute, and use the ````TryUpdateModelAsync```` method. 
This method attempts to write only the updated information or fields (user submitted form data/input) to an existing entity. In this scenario, the entity is called
directly from the context, and not as a bound instance with user submitted data. Then, using the _TryUpdateModelAsync_ method, this context instance is updated with
the fields that you desire to be changed (ONLY). How?

Entity Framework's (EF) automatic change tracking sets the Modified flag on the _fields that are changed by form input_. When the ````SaveChanges```` method 
is called EF creates SQL statements to update the database row. Concurrency conflicts are ignored, and only the table columns that were updated by the user 
are updated in the database!

While the method can be used without fields specified -- it is a best practice to include the fields that you want to be updateable or declare them in the ````TryUpdateModel```` 
parameters as this also prevents overposting (The empty string preceding the list of fields in the parameter list is for a prefix to use with the form fields names). Currently 
there are no extra fields that you're protecting, but listing the fields that you want the model binder to bind ensures that if you add fields to the data model in the future, 
they're automatically protected until you explicitly add them here. [To learn more, see the ASP .Net Documentation](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-6.0).



### Read and update (Alternative) - No-tracking queries ###

When a database context retrieves table rows and creates entity objects that represent them, by default it keeps track of whether the entities in memory are in sync with what's 
in the database. The data in memory acts as a cache and is used when you update an entity. This caching is often unnecessary in a web application because context instances are
typically short-lived (a new one is created and disposed for each request) and the context that reads an entity is typically disposed before that entity is used again. You can 
disable tracking of entity objects in memory by calling the ````AsNoTracking```` method. Typical scenarios in which you might want to do that include the following:

* During the context lifetime you don't need to update any entities, and you don't need EF to automatically load navigation properties with entities retrieved by separate queries.
Frequently these conditions are met in a controller's HttpGet action methods.
* You are running a query that retrieves a large volume of data, and only a small portion of the returned data will be updated. It may be more efficient to turn off tracking for 
the large query, and run a query later for the few entities that need to be updated.
* You want to attach an entity in order to update it, but earlier you retrieved the same entity for a different purpose. Because the entity is already being tracked by the 
database context, you can't attach the entity that you want to change. One way to handle this situation is to call ````AsNoTracking```` on the earlier query.


An example follows:

    var CreatedPersistence = _context.Entity.AsNoTracking().Single(m => m.Id == id).SomeAttribute;
    if (CreatedPersistence != null)
    {
        entityToBeSaved.SomeAttribute = CreatedPersistence;
    }
    entityToBeSaved.AnotherAttribute = UpdatedInformation;
    _context.Update(entityToBeSaved);
    await _context.SaveChangesAsync();


This allows me to retrive the currently saved version of an Entity (````_context.Entity````) and access its properties or attributes (````SomeAttribute````) that I wish to persist 
(````entityToBeSaved.SomeAttribute = CreatedPersistence;````) along with the user submitted data (````entityToBeSaved.AnotherAttribute = UpdatedInformation;```` thus constituting a 
new version of the same entity). Since, it is NOT TRACKED, I will not encounter concurrency conflicts (having two of the same object opened in memory).



## Search and Order ##

This application cannot both search and order. Only one of the given functions works at any point in time.



## Entity Framework Database Manipulation ##

```` dotnet ef database drop ```` Allows you to drop the database presently linked to your application, via the appsettings.json file.

```` dotnet ef database update ```` Allows you to recreate a database using your present context file.



## Load Related Data ##

* Eager loading:  using ````Include()```` and ````ThenInclude()````` when an entity is loaded, the related data can then be strung along with it. Or, you can query a context using the information from an already selected entity (thus applying ```Load()``` to search that entity for its dependent or linked entities).
* Explicit loading: is similar to the latter part of eager loading and uses the exact same keyword.
* Lazy loading: using the navigation property of a loaded entity, one has access to related data



## Delete Related Data ##

* Using instructors as a model for deletion:
    + see taht it uses data loading methods mentioned above (especially eager loading) to connect to related data and then deletes it in its controller. A suggested alternative from the documentation is using Entity Framework's (EF) ```` OnDelete ```` method to configure _cascade delete_.
    + whereas the department is explicitly loaded on its own not as related data.



## Completed Items ##

* Basic: Course, CourseAssignment, Department, Enrollment, Instructor, OfficeAssignment, Student
* InstructorIndexData: a model created for the instructors index page to contain instructor and related data.
* EnrollmentDateGroup: a model created for the home controller and about page
* PaginatedList: allows for page indexing if list of entries exceeds 8 data records - applied to all page indexes
* 



## Resources ##

* [Entity Framework](https://docs.microsoft.com/en-us/ef/)
* [.Net](https://docs.microsoft.com/en-us/dotnet/)
* [ASP .Net](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-6.0)
* [NuGet](https://docs.microsoft.com/en-us/nuget/)
* [C#](https://docs.microsoft.com/en-us/dotnet/csharp/)
* [Visual Studio](https://docs.microsoft.com/en-us/visualstudio/?view=vs-2022)
* [SQL Server](https://docs.microsoft.com/en-us/sql/?view=sql-server-ver15)
* [GitHub and Git](https://docs.github.com/en/get-started)
* [MarkDown on Github](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)
* [Online Regex](https://regex101.com/r/DDrgWh/1)
* [Online C# Compiler](https://dotnetfiddle.net/)
* [Bootstrap Introduction](https://docs.microsoft.com/en-us/archive/msdn-magazine/2015/june/web-development-building-responsive-web-sites-with-bootstrap)
* [Bootstrap v5.x](https://getbootstrap.com/)
* [MSDN Magazines](https://docs.microsoft.com/en-us/archive/msdn-magazine/msdn-magazine-issues)